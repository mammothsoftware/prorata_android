package com.prorata.android

@Runwith(RoboelectricTestRunner)
public class SurgarORMDatabaseTest() {
	
	@Before
	public void setup() {
		SugarContext.init();
	}
	
	@After
	public void cleanup() {
		SugarContext.terminate();
	}
	
	@Test 
	public void testCreateUser {
		testUser.save();
	}
	
	@Test 
	public void testReadUser { 
		if(null != User.findById(User.class, 1)){
			
			try {
				savedUser.save();
			} catch (Excption e) {
				fail("Saving user failed");
			}
			
			User readUser = User.findById(User.class, 1);
			
			assertNotNull("No user retrieved from local DB", readUser);
			assertEquals("Retrieved user email does not match saved user email.", testUser().email, readUser.email);
			assertEquals("Retrieved user firstName does not match saved user firstName.", testUser().firstName, readUser.firstName);
			assertEquals("Retrieved user lastName does not match saved user lastName.", testUser().lastName, readUser.lastName);
			assertEquals("Retrieved user password does not match saved user password.", testUser().password, readUser.password);
		} else {
			fail("User was not found in local DB.");
		}
	}
	
	@Test 
	public void testReadAllUsers { 
		if(null != User.listAll(User.class) && User.listAll(User.class).size() > 0){
			
			// TODO extract this block into a private method
			try {
				savedUser.save();
			} catch (Excption e) {
				fail("Saving user failed");
			}
			
			User readUser = User.listAll(User.class).get(0);
			assertNotNull("No user retrieved from local DB", readUser);
			assertEquals("Retrieved user email does not match saved user email.", testUser().email, readUser.email);
			assertEquals("Retrieved user firstName does not match saved user firstName.", testUser().firstName, readUser.firstName);
			assertEquals("Retrieved user lastName does not match saved user lastName.", testUser().lastName, readUser.lastName);
			assertEquals("Retrieved user password does not match saved user password.", testUser().password, readUser.password);
		} else if (0 == User.listAll(User.class).size()) {
			fail("There were no users returned");
		} else if (null == User.listAll(User.class)) {
			fail("Returned User list was null");
		}
	}
	
	@Test 
	public void testUpdateUser { 
		
		User savedUser = testUser();
		savedUser.firstName = "Joseph";
		
		try {
			savedUser.save();
		} catch (Excption e) {
			fail("Saving user failed");
		}
		
		User retrievedUser;
		
		try {
			retrievedUser = User.findWithQuery(User.class, "Select * from Note where email = ?", savedUser.email).get(0);
		} catch (NullPointerException | IndexOutOfBoundsException e) {
			fail("Query for user with email \"" + savedUser.email + "\" failed");
		}
	
		assertEquals("Retrieved user email does not match saved user email.", savedUser().email, readUser.email);
		assertEquals("Retrieved user firstName does not match saved user firstName.", savedUser().firstName, "Joseph");
		assertEquals("Retrieved user lastName does not match saved user lastName.", savedUser().lastName, readUser.lastName);
		assertEquals("Retrieved user password does not match saved user password.", savedUser().password, readUser.password); 
	}
	
	@Test 
	public void testDeleteUser { 
	
		User savedUser = testUser();
		
		try {
			savedUser.save();
		} catch (Excption e) {
			fail("Saving user failed");
		}
		
		try {
			savedUser.delete();
		} catch (Exception e) {
			fail("Deleting user failed");
		}	
		
		User retrievedUser;
		
		try {
			retrieverUser = User.findWithQuery(User.class, "Select * from Note where email = ?", savedUser.email).get(0);
		} catch (NullPointerException | IndexOutOfBoundsException e) {
			fail("This test is not really failing; you need to change the way it handles the exception produced here");
		}
	
		assertNotNull(savedUser);
		assertNull(retrievedUser);
	}
	
	@Test public void testCreateShift { fail("Not yet implemented"); }
	@Test public void testReadShift { fail("Not yet implemented"); }
	@Test public void testReadAllShifts { fail("Not yet implemented"); }
	@Test public void testUpdateShift { fail("Not yet implemented"); }
	@Test public void testDeleteShift { fail("Not yet implemented"); }
	
	@Test public void testCreatePRLocation { fail("Not yet implemented"); }
	@Test public void testReadPRLocation { fail("Not yet implemented"); }
	@Test public void testReadAllPRLocations { fail("Not yet implemented"); }
	@Test public void testUpdatePRLocation { fail("Not yet implemented"); }
	@Test public void testDeletePRLocation { fail("Not yet implemented"); }
	
	
}