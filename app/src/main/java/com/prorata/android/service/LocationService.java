package com.prorata.android.service;

public class LocationService {
    private static final LocationService ourInstance = new LocationService();

    public static LocationService getInstance() {
        return ourInstance;
    }

    private LocationService() {
    }
}
